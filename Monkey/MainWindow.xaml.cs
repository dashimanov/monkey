﻿using System.Windows;

namespace Monkey
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CheckDb checkDb = new CheckDb();
            checkDb.Show();
        }
    }
}
