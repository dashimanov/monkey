﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using Monkey.Classes;

namespace Monkey
{
    /// <summary>
    /// Логика взаимодействия для CheckDb.xaml
    /// </summary>
    public partial class CheckDb : Window
    {
        public CheckDb()
        {
            InitializeComponent();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            
            GetpcName name = new GetpcName();
            SqlConnection connection = new SqlConnection("Server = localhost; "
                                                         + "Initial Catalog = " + name.NamePc() + ";"
                                                         + "Integrated Security = SSPI");

            SqlCommand command = new SqlCommand
            {
                CommandText = "dbcc checkdb ('" + name.NamePc() + "')",
                CommandType = CommandType.Text,
                Connection = connection
            };

            try
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    object percent = reader.GetValue(2);
                    LabelTime.Content = percent.ToString();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}
