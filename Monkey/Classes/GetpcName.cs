﻿using System;

namespace Monkey.Classes
{
    public class GetpcName
    {
        public string NamePc()
        {
            string name = Environment.MachineName.ToLower();

            string[] index = name.Split('-');
            string dbName = "DB" + index[1];

            return dbName;
        }
    }
}
